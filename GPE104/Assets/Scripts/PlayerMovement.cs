﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    //Changes the speed of the ship depending on what value is assigned.
    public float Speed = 10;

    //Declares Rigidbody2d
    private Rigidbody2D rb2d;

    private void Start() {
        //Gets the Rigidbody2D compenent and sets it
        rb2d = GetComponent<Rigidbody2D>();
    }

    private void Update() {
        //This moves the player by the factor of 'Speed' in it's forward direction which is 'transform.up' in a 2D case
        rb2d.velocity = transform.up * Speed;
    }
}
